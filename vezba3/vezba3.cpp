/*
 * Vezba 3
 * Zadatak 3
 *
 * Built and tested on:
 *  Compiler: GCC 7.2.0 on Arch Linux
 *  Architecture: amd64
 *  Standard: C++ 17
 */

#include <stdio.h>

#define MAX_ARR 200

int main(int argc, char *argv[]) {
    printf("Enter string: ");
    char arr[MAX_ARR];
    char c;
    int flag = 0;
    int i = 0;
    while((c = fgetc(stdin)) != '\n') {
        if (i >= MAX_ARR) break;
        if (c == '(') {
            flag = 1;
            continue;
        } else if (c == ')' && flag == 1) {
            flag = 0;
            continue;
        } else if (flag == 0) {
            arr[i++] = c;
        }
    }

    arr[i] = '\0';
    printf("\n%s\n", arr);
    return 0;
}