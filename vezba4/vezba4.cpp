/*
 * Vezba 4
 * Zadatak 2
 *
 * Built and tested on:
 *  Compiler: GCC 7.2.0 on Arch Linux
 *  Architecture: amd64
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct Student {
    char *ime_studenta;
    char *ime_smera;
    int br_indeksa;
    int god_upisa;
} Student;

#define BUFFER_SIZE 10

void checkCharMemoryAllocation(char *buffer) {
    if (buffer == NULL) {
        fprintf(stderr, "Memory allocation error. Exiting.");
        exit(2);
    }
}

char *readLine() {
    char *buffer;
    uint buffer_size = 100;
    char c;
    int i = 0;

    buffer = (char *) calloc(buffer_size, sizeof(char));
    checkCharMemoryAllocation(buffer);

    while ((c = fgetc(stdin)) != '\n') {
        if ((i % 100) == 0) {
            // Reallocating memory to expand it
            buffer = (char *) realloc(buffer, (i + buffer_size) * sizeof(char));
            checkCharMemoryAllocation(buffer);
        }
        buffer[i++] = c;
    }

    // Resizing the memory to minimum required size and adding string delimiter at the end.
    buffer[i] = '\0';
    buffer = (char *) realloc(buffer, i * sizeof(char));

    return buffer;
}

void readStudentList(Student *sp, int n) {
    printf("Enter student information.");
    char eatNewLine;
    for (int i = 0; i < n; i++) {
        if ((i % BUFFER_SIZE) == 0) {
            // Reallocating memory to expand it
            sp = (Student *) realloc(sp, (i + BUFFER_SIZE) * sizeof(Student));
            if (sp == NULL) {
                fprintf(stderr, "Memory allocation error. Exiting.\n");
                exit(1);
            }
        }
        printf("\n---------------------\nStudent name: ");
        sp[i].ime_studenta = readLine();
        printf("Program name: ");
        sp[i].ime_smera = readLine();
        printf("Index: ");
        scanf("%d", &sp[i].br_indeksa);
        printf("Year enrolled: ");
        scanf("%d", &sp[i].god_upisa);
        eatNewLine = fgetc(stdin);
    }

}

int main(int argc, const char *argv[]) {
    char eatNewLine;
    Student *sp = (Student *) calloc(BUFFER_SIZE, sizeof(Student));
    if (sp == NULL) {
        fprintf(stderr, "Memory allocation error. Exiting.\n");
        exit(1);
    }

    printf("How many students do you want to enter: ");
    int n = 0;
    scanf("%d", &n);
    eatNewLine = fgetc(stdin);

    readStudentList(sp, n);

    int enroll_limit = 0;
    printf("Upper limit for enrollment year: ");
    scanf("%d", &enroll_limit);


    for (int i = 0; i < n; i++) {
        if (sp[i].god_upisa < enroll_limit) {
            printf("\nStudent name: %s | Program: %s | Index: %d | Enrolled: %d", sp[i].ime_studenta, sp[i].ime_smera,
                   sp[i].br_indeksa, sp[i].god_upisa);
        }
    }

    return 0;
}