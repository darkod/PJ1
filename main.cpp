/*
 * Vezba 3
 * Zadatak 3
 *
 * Built and tested on:
 *  Compiler: GCC 7.2.0 on Arch Linux
 *  Architecture: amd64
 *  Standard: C++ 17
 */

#include <iostream>
#include <cstring>

class User {
public:
    std::string name;
    std::string pin;
    double balance;

    User(const std::string &name, const std::string &pin, double balance) : name(name), pin(pin), balance(balance) {
        this->name = name;
        this->pin = pin;
        this->balance = balance;
        std::cout << "Welcome " << this->name << std::endl;
    };
};

int main(int argc, char *argv[]) {

    std::string name;
    std::string pin;
    double balance;

    std::cout << "Provide user information\n";
    std::cout << "Users name: ";
    std::cin >> name;
    std::cout << "Pin: ";
    std::cin >> pin;
    std::cout << "Balance: ";
    std::cin >> balance;
    User usr(name, pin, balance);

    printf("\nReading Balance.\nPlease provide pin number: ");
    pin = "0"; // Resetting PIN
    std::cin >> pin;
    if (pin == usr.pin) {
        std::cout << "Pin correct. Balance is " << usr.balance << std::endl;
    } else {
        std::cout << "Wrong pin provided." << std::endl;
    }
}