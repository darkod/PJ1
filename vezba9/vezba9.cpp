/*
 * Vezba 9
 * Zadatak 3
 *
 * Built and tested on:
 *  Compiler: GCC 7.2.0 on Arch Linux
 *  Architecture: amd64
 *  Standard: C++ 17
 */

#include <iostream>

using namespace std;


int main(int argc, char *argv[]) {

    FILE *fp = fopen("../main.cpp", "r");
    FILE *nf = fopen("output.txt", "w+");

    int c;
    int i = 1;
    while ((c = fgetc(fp)) != EOF) {
        if (c == '\n') {
            fprintf(nf, "\n%d\t",i++);
        } else if (i == 1) {
            fprintf(nf, "%d\t%c", i++, c);
        }
        else {
            fprintf(nf, "%c", c);
        }
    }

}