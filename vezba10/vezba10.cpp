/*
 * Vezba 10
 * Zadatak 2
 *
 * Built and tested on:
 *  Compiler: GCC 7.2.0 on Arch Linux
 *  Architecture: amd64
 *  Standard: C++ 17
 */

/*
 * Note: Nije mi bas najjasnije sta se trazi u zadatku i kako da se to implementira...
 */

#include <iostream>
#include <list>

using namespace std;

class User {
public:
    string name;
    string address;
    string country;

    User(string name, string address, string country) {
        this->name = name;
        this->address = address;
        this->country = country;
    }
};

class Vehicle {
public:
    string model;
    string type;
    uint power;
    uint age;

    Vehicle(string model, string type, uint power, uint age) {
        this->model = model;
        this->type = type;
        this->power = power;
        this->age = age;
    }
};

void report(list<User> userList,list<Vehicle> vehicleList) {

    cout << "Vehicle Report: " << endl;

    for (Vehicle n : vehicleList) {
        std::cout << n.model << " " << n.type << " " << n.power << " "  << n.age << '\n';
    }

    cout << "User Report: " << endl;

    for (User n : userList) {
        std::cout << n.name << " " << n.address << " " << n.country << '\n';
    }
}

int main(int argc, char *argv[]) {
    list<User> userList;
    list<Vehicle> vehicleList;
    cout << "How many users you want to store: ";
    uint usrCount = 0;
    cin >> usrCount;
    User temp("", "", "");
    char clear = fgetc(stdin);
    for (int i = 0; i < usrCount; i++) {
        cout << "Users name: ";
        getline(cin, temp.name);
        cout << "Users address: ";
        getline(cin, temp.address);
        cout << "Users country: ";
        getline(cin, temp.country);
        User usr(temp.name, temp.address, temp.country);
        cout << usr.name << " " << usr.address << " " << usr.country << endl;
        userList.push_back(usr);
    }

    cout << "Number of Vehicles to store: ";
    uint vehicleCount = 0;
    cin >> vehicleCount;
    Vehicle tempV("", "", 0, 0);
    clear = fgetc(stdin);
    for (int i = 0; i < vehicleCount; i++) {
        cout << "Vehicle model: ";
        getline(cin, tempV.model);
        cout << "Vehicle type: ";
        getline(cin, tempV.type);
        cout << "Vehicle power: ";
        cin >> tempV.power;
        cout << "Vehicle age: ";
        cin >>  tempV.age;
        clear = fgetc(stdin); // To clear last enter
        Vehicle vehicle(tempV.model, tempV.type ,tempV.power, tempV.age);
        cout << vehicle.model << " " << vehicle.type << " " << vehicle.power << " " << vehicle.age << endl;
        vehicleList.push_back(vehicle);
    }
    
    report(userList, vehicleList);

    return 0;
}
