/*
 * Vezba 8
 * Zadatak 3
 *
 * Built and tested on:
 *  Compiler: GCC 7.2.0 on Arch Linux
 *  Architecture: amd64
 *  Standard: C++ 17
 */

#include <iostream>

#define MAX_ARR_LENGTH 20

using namespace std;

int readArray(int *ap) {

    int c;
    int i = 0;
    char temp = fgetc(stdin);
    uint bufferSize = MAX_ARR_LENGTH;
    int n;
    cout << "What is number of integers you want to store, max is " << MAX_ARR_LENGTH << " : ";
    cin >> n;
    if (n > MAX_ARR_LENGTH) {
        fprintf(stderr, "Maximum number of elements is %d", MAX_ARR_LENGTH);
    }
    cout << "Enter Array elements" << endl;
    for (i = 0; i < n; i++) {
        cout << "Enter number " << i + 1 << ": ";
        cin >> c;
        if ((i % bufferSize) == 0 && i != 0) {
            // We need to reallocate it
            ap = (int *) realloc(ap, (i + bufferSize) * sizeof(int));
            if (ap == NULL) {
                perror("Memory allocation error: ");
                exit(2);
            }
        }
        ap[i] = c;
    }

    ap = (int *) realloc(ap, i * sizeof(int));
    if (ap == NULL) {
        perror("Memory allocation error: ");
        exit(2);
    }

    return n;
}

int compressArray(int *ap, int arrSize) {
    int newArraySize = arrSize;
    for (int i = 0; i < arrSize; i++) {
        if (ap[i] == 0) {
            // If 0 is found shift array to that element.
            for (int j = i; j < arrSize; j++) {
                if ((j + 1) == arrSize) break;
                ap[j] = ap[j + 1];
            }
            if ((i + 1) == arrSize && ap[arrSize - 1] == 0) {
                continue;
            } else {
                newArraySize--;
            }

        }
    }
    ap = (int *) realloc(ap, newArraySize * sizeof(int));
    return newArraySize;
}

int expandArray(int *ap, int arrSize) {
    cout << "Append additional numbers to array." << endl;
    int *newArray = (int *) calloc(MAX_ARR_LENGTH, sizeof(int));
    if (newArray == NULL) {
        perror("Memory allocation error: ");
        exit(2);
    }
    int newArraySize = readArray(newArray);

    ap = (int *) realloc(ap, (arrSize + newArraySize) * sizeof(int));
    copy(newArray, newArray + newArraySize, &ap[arrSize]);

    return (arrSize + newArraySize);
}

void showArray(int *ap, int arrSize) {
    for (int i = 0; i < arrSize; i++) {
        cout << "| " << ap[i] << " |";
    }
    cout << "\nArray size: " << arrSize << endl;
}

int main(int argc, char *argv[]) {

    cout <<
         "1. Citanje niza\n" << "2. Sazimanje niza\n" << "3. Prosirenje niza\n" << "4. Prikaz niza\n" << "5. Izlaz\n"
         << endl;

    int option;

    uint bufferSize = MAX_ARR_LENGTH;
    int *ap = (int *) calloc(bufferSize, sizeof(int));
    if (ap == NULL) {
        perror("Memory allocation error: ");
        exit(1);
    }

    int arrSize = 0;
    while (1) {
        cout << "Option: ";
        cin >> option;
        switch (option) {
            case 1:
                arrSize = readArray(ap);
                break;
            case 2:
                arrSize = compressArray(ap, arrSize);
                break;
            case 3:
                arrSize = expandArray(ap, arrSize);
                break;
            case 4:
                showArray(ap, arrSize);
                break;
            case 5:
                cout << "Bye Bye!";
                free(ap);
                exit(1);
            default:
                cout << "No option provided. Use 5 for exit." << endl;
                free(ap);
        }
    }


}