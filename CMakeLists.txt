cmake_minimum_required(VERSION 3.8)
project(programskiJezici)

set(CMAKE_CXX_STANDARD 17)

set(SOURCE_FILES main.cpp main.cpp)
add_executable(programskiJezici ${SOURCE_FILES})