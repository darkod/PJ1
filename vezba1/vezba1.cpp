/*
 * Vezba 1
 * Zadatak 4
 *
 * Built and tested on:
 *  Compiler: GCC 7.2.0 on Arch Linux
 *  Architecture: amd64
 */

#include <stdio.h>
#include <stdlib.h>

int main(int argc, const char *argv[]) {
    if (argc == 1) {
        fprintf(stderr, "No arguments passed, exiting.\n");
        exit(1);
    }

    double sum = 0; // Sum
    double avr = 0; // Average
    double tmp;     // Temporary variable

    // We go from number 1 as argv[0] is program name
    for (int i = 1; i < argc; i++) {
        tmp = atof(argv[i]);
        sum += tmp;
    }

    avr = sum / (argc - 1); // Eliminate program name with -1

    printf("Sum: %lf\nAverage: %lf\n", sum, avr);

    return 0;
}