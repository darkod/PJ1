/*
 * Vezba 2
 * Zadatak 2
 *
 * Built and tested on:
 *  Compiler: GCC 7.2.0 on Arch Linux
 *  Architecture: amd64
 */

#include <stdio.h>
#include <stdlib.h>

int getNumberCount(int *num) {
    printf("Provide number count: ");
    scanf("%d", num);

    if (num == 0) {
        fprintf(stderr, "No numbers can be provided with count set to 0. Exiting.");
        exit(1);
    }
}

void getNumberArray(int arr[], int num) {
    printf("Provide %d numbers\n", num);

    for (int i = 0; i < num; i++) {
        printf("%d. number: ", i + 1);
        scanf("%d", &arr[i]);
    }
}

void getSortOrder(char *order) {
    printf("What order do you want array to be sorted (a - ascending / d - descending): ");
    scanf("%c", order);
    scanf("%c", order);
}

void swap(int *x, int *y) {
    int temp = *x;
    *x = *y;
    *y = temp;
}

void sortArray(char order, int arr[], int num) {
    if (order == 'a' || order == 'A') {
        for (int i = 0; i < num - 1; i++) {
            // Last i elements are already in place
            for (int j = 0; j < num - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    swap(&arr[j], &arr[j + 1]);
                }
            }
        }
    } else if (order == 'd' || order == 'D') {
        for (int i = 0; i < num - 1; i++) {
            // Last i elements are already in place
            for (int j = 0; j < num - i - 1; j++) {
                if (arr[j] < arr[j + 1]) {
                    swap(&arr[j], &arr[j + 1]);
                }
            }
        }
    }
}

void printArrayToStdout(int num, int arr[]) {
    printf("Sorted array\n");
    for (int i = 0; i < num; i++) {
        printf("%d. number: %d\n", i + 1, arr[i]);
    }
}

int main(int argc, const char *argv[]) {

    int num = 0;
    getNumberCount(&num);

    int arr[10];
    getNumberArray(arr, num);

    char order;
    getSortOrder(&order);

    sortArray(order, arr, num);

    printArrayToStdout(num, arr);

    return 0;
}