/*
 * Vezba 6
 * Zadatak 4*
 *
 * Built and tested on:
 *  Compiler: GCC 7.2.0 on Arch Linux
 *  Architecture: amd64
 */

#include <stdio.h>
#include <stdlib.h>

void checkCharMemoryAllocation(char *buffer) {
    if (buffer == NULL) {
        fprintf(stderr, "Memory allocation error. Exiting.");
        exit(2);
    }
}

char *readLine() {
    char *buffer;
    uint buffer_size = 100;
    char c;
    int i = 0;

    buffer = (char *) calloc(buffer_size, sizeof(char));
    checkCharMemoryAllocation(buffer);

    while ((c = fgetc(stdin)) != '\n') {
        if ((i % 100) == 0) {
            // Reallocating memory to expand it
            buffer = (char *) realloc(buffer, (i + buffer_size) * sizeof(char));
            checkCharMemoryAllocation(buffer);
        }
        buffer[i++] = c;
    }

    // Resizing the memory to minimum required size and adding string delimiter at the end.
    buffer[i] = '\0';
    buffer = (char *) realloc(buffer, i * sizeof(char));

    return buffer;
}

int main(int argc, const char *argv[]) {

    FILE *fp1 = NULL;
    FILE *fp2 = NULL;
    FILE *fp3 = NULL;
    printf("Enter file path: ");
    char *fileName = readLine();

    //Opening the file
    fp1 = fopen(fileName, "r");
    fp2 = fopen("output-coded.txt", "w+");
    fp3 = fopen("output-decoded.txt", "w+");

    int c;
    // Encoding the file from fp1 to fp2
    printf("Encoding file 1 into file 2.\n");
    while ((c = fgetc(fp1)) != EOF) {
        if (c == '_') {
            fprintf(fp2, "__");
        } else {
            fprintf(fp2, "%c", c);
        }
    }

    // Reset pointer to beginning of the file 2
    fseek(fp2, 0, SEEK_SET);

    int lastChar;
    printf("Decoding file 2 into file 3.\n");
    while ((c = fgetc(fp2)) != EOF) {
        if (c == '_' && lastChar == '_') {
            continue;
        } else {
            lastChar = c;
            fprintf(fp3, "%c", c);
        }
    }

    fclose(fp1);
    fclose(fp2);
    fclose(fp3);
    return 0;
}