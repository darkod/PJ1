/*
 * Vezba 7
 * Zadatak 2
 *
 * Built and tested on:
 *  Compiler: GCC 7.2.0 on Arch Linux
 *  Architecture: amd64
 *  Standard: C++ 17
 */

#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {

    double sum(0);
    double max(0);
    double min(0);

    if (argc > 6) {
        cout << "Maximum number of characters exided. Only using first 5" << endl;
    }

    for (int i = 1; i < argc && i < 6; i++) {
        if (i == 1) {
            min = max = atof(argv[i]);
        } else {
            if (atof(argv[i]) > max) {
                max = atof(argv[i]);
            } else if (atof(argv[i]) < min) {
                min = atof(argv[i]);
            }
        }
        sum += atof(argv[i]);
    }

    cout << "Sum of all numbers is: " << sum << "| Max is: " << max << "| Min is: " << min << endl;

    return 0;
}