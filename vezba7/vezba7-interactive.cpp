/*
 * Vezba 7
 * Zadatak 2
 *
 * Built and tested on:
 *  Compiler: GCC 7.2.0 on Arch Linux
 *  Architecture: amd64
 *  Standard: C++ 17
 */

#include <iostream>

using namespace std;

int main() {

    double arr[5];
    int n = 0;
    double sum(0);
    double max(0);
    double min(0);

    cout << "How many numbers you want to enter: ";
    cin >> n;

    for (int i = 0; i < n; i++) {
        cout << i + 1 << ". Number: ";
        cin >> arr[i];
        if (i == 0) {
            min = max = arr[i];
        } else {
            if (arr[i] > max) {
                max = arr[i];
            } else if (arr[i] < min) {
                min = arr[i];
            }
        }
        sum += arr[i];
    }

    cout << "Sum of all numbers is: " << sum << "| Max is: " << max << "| Min is: " << min << endl;

    return 0;
}