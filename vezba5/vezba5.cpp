/*
 * Vezba 5
 * Zadatak 2
 *
 * Built and tested on:
 *  Compiler: GCC 7.2.0 on Arch Linux
 *  Architecture: amd64
 */

#include <stdio.h>
#include <stdlib.h>

void checkCharMemoryAllocation(char *buffer) {
    if (buffer == NULL) {
        fprintf(stderr, "Memory allocation error. Exiting.");
        exit(2);
    }
}

char *readLine() {
    char *buffer;
    uint buffer_size = 100;
    char c;
    int i = 0;

    buffer = (char *) calloc(buffer_size, sizeof(char));
    checkCharMemoryAllocation(buffer);

    while ((c = fgetc(stdin)) != '\n') {
        if ((i % 100) == 0) {
            // Reallocating memory to expand it
            buffer = (char *) realloc(buffer, (i + buffer_size) * sizeof(char));
            checkCharMemoryAllocation(buffer);
        }
        buffer[i++] = c;
    }

    // Resizing the memory to minimum required size and adding string delimiter at the end.
    buffer[i] = '\0';
    buffer = (char *) realloc(buffer, i * sizeof(char));

    return buffer;
}

int main(int argc, const char *argv[]) {

    FILE *fp1 = NULL;
    FILE *fp2 = NULL;
    printf("Enter file path: ");
    char *fileName = readLine();

    //Opening the file
    fp1 = fopen(fileName, "r");
    fp2 = fopen("output.txt", "w+");
    int c;
    while ((c = fgetc(fp1)) != EOF) {
        if (c == ' ' || c == '\n') {
            fprintf(fp2, "\n");
        } else {
            fprintf(fp2, "%c", c);
        }
    }

    fclose(fp1);
    fclose(fp2);
    return 0;
}